package edu.rit.cs;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.lit;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataTypes;
import java.io.File;

public class DiversityIndex_Spark {
  // If you run this example within IntelliJ IDE, you need to point the working directory to `word_count`

  public static final String OutputDirectory = "us_census_diversity_index/dataset/review-outputs";
  public static final String DatasetFile = "us_census_diversity_index/dataset/cc-est2017-alldata.csv";

  public static boolean deleteDirectory(File directoryToBeDeleted) {
    File[] allContents = directoryToBeDeleted.listFiles();
    if (allContents != null) {
      for (File file : allContents) {
        deleteDirectory(file);
      }
    }
    return directoryToBeDeleted.delete();
  }

  public static void diversityIndex(SparkSession spark) {
    Dataset ds = spark.read()
        .option("header", "true")
        .option("delimiter", ",")
        .option("inferSchema", "true")
        .csv(DatasetFile);

    // Encoders are created for Java beans
    Encoder<CensusData> reviewEncoder = Encoders.bean(CensusData.class);
    Dataset<CensusData> ds1 = ds.as(reviewEncoder);
    //ds1.show();

    Dataset ds2 = ds1.filter(col("AGEGRP").equalTo(0))
        .select(col("STNAME").cast(DataTypes.StringType), col("CTYNAME").cast(DataTypes.StringType),
            col("YEAR"), col("AGEGRP"), col("TOT_POP"), col("WA_MALE").cast(DataTypes.LongType),
            col("WA_FEMALE").cast(DataTypes.DoubleType), col("BA_MALE").cast(DataTypes.DoubleType),
            col("BA_FEMALE").cast(DataTypes.DoubleType), col("IA_MALE").cast(DataTypes.DoubleType),
            col("IA_FEMALE").cast(DataTypes.DoubleType), col("AA_MALE").cast(DataTypes.DoubleType),
            col("AA_FEMALE").cast(DataTypes.DoubleType), col("NA_MALE").cast(DataTypes.DoubleType),
            col("NA_FEMALE").cast(DataTypes.DoubleType), col("TOM_MALE").cast(DataTypes.DoubleType),
            col("TOM_FEMALE").cast(DataTypes.DoubleType));
    //ds2.show();

    Dataset ds3 = ds2
        .withColumn("STNAME", col("STNAME"))
        .withColumn("CTYNAME", col("CTYNAME"))
        .withColumn("YEAR", col("YEAR"))
        .withColumn("WA_SUM", col("WA_MALE").plus(col("WA_FEMALE")))
        .withColumn("BA_SUM", col("BA_MALE").plus(col("BA_FEMALE")))
        .withColumn("IA_SUM", col("IA_MALE").plus(col("IA_FEMALE")))
        .withColumn("AA_SUM", col("AA_MALE").plus(col("AA_FEMALE")))
        .withColumn("NA_SUM", col("NA_MALE").plus(col("NA_FEMALE")))
        .withColumn("TOM_SUM", col("TOM_MALE").plus(col("TOM_FEMALE")))
        .withColumn("TOT_SUM",
            col("WA_SUM").plus(col("BA_SUM")).plus(col("IA_SUM")).plus(col("AA_SUM"))
                .plus(col("NA_SUM")).plus(col("TOM_SUM")))
        .drop("YEAR", "AGEGRP", "WA_MALE", "WA_FEMALE", "BA_MALE", "BA_FEMALE", "IA_MALE",
            "IA_FEMALE", "AA_MALE", "AA_FEMALE", "NA_MALE", "NA_FEMALE", "TOM_MALE", "TOM_FEMALE")
        .groupBy("STNAME", "CTYNAME")
        .sum()
        .orderBy("STNAME", "CTYNAME");
    //ds3.show();

    Dataset ds4 = ds3
        .withColumn("1/T2", lit(1).divide(col("sum(TOT_SUM)").multiply(col("sum(TOT_SUM)"))))
        .withColumn("SIG_WA",
            col("sum(WA_SUM)").multiply(col("sum(TOT_SUM)").minus(col("sum(WA_SUM)"))))
        .withColumn("SIG_BA",
            col("sum(BA_SUM)").multiply(col("sum(TOT_SUM)").minus(col("sum(BA_SUM)"))))
        .withColumn("SIG_IA",
            col("sum(IA_SUM)").multiply(col("sum(TOT_SUM)").minus(col("sum(IA_SUM)"))))
        .withColumn("SIG_AA",
            col("sum(AA_SUM)").multiply(col("sum(TOT_SUM)").minus(col("sum(AA_SUM)"))))
        .withColumn("SIG_NA",
            col("sum(NA_SUM)").multiply(col("sum(TOT_SUM)").minus(col("sum(NA_SUM)"))))
        .withColumn("SIG_TOM",
            col("sum(TOM_SUM)").multiply(col("sum(TOT_SUM)").minus(col("sum(TOM_SUM)"))))
        .withColumn("SIG_ALL", col("SIG_WA").plus(col("SIG_BA").plus(col("SIG_IA")
            .plus(col("SIG_AA").plus(col("SIG_NA").plus(col("SIG_TOM")))))))
        .withColumn("DIVERSITY_INDEX", col("1/T2").multiply(col("SIG_ALL")))
        .drop("WA_SUM", "BA_SUM", "IA_SUM", "AA_SUM", "NA_SUM", "TOM_SUM", "sum(WA_SUM)",
            "sum(BA_SUM)", "sum(IA_SUM)", "sum(AA_SUM)", "sum(NA_SUM)", "sum(TOM_SUM)",
            "sum(TOT_POP)", "sum(TOT_SUM)", "SIG_WA", "SIG_BA", "SIG_IA", "SIG_AA", "SIG_NA",
            "SIG_TOM", "1/T2", "SIG_ALL");
    ds4.show();

//    Dataset ds5 = ds4
//        .select("STNAME", "CTYNAME", "DIVERSITY_INDEX");
//    ds5.show();

    deleteDirectory(new File(OutputDirectory + "_RESULTS"));
    ds4.repartition(1).write().format("csv").option("header", "true").save(OutputDirectory+"_RESULTS");
  }

  public static void main(String[] args) throws Exception {
    // Create a SparkConf that loads defaults from system properties and the classpath
    SparkConf sparkConf = new SparkConf();
    sparkConf.set("spark.master", "local[8]");
    //Provides the Spark driver application a name for easy identification in the Spark or Yarn UI
    sparkConf.setAppName("Diversity Index");

    // Creating a session to Spark. The session allows the creation of the
    // various data abstractions such as RDDs, DataFrame, and more.
    SparkSession spark = SparkSession.builder().config(sparkConf).getOrCreate();

    // Creating spark context which allows the communication with worker nodes
    JavaSparkContext jsc = new JavaSparkContext(spark.sparkContext());

    diversityIndex(spark);

    // Stop existing spark context
    jsc.close();

    // Stop existing spark session
    spark.close();
  }
}
