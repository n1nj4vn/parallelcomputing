Prerequisites:

1. Place the omp4j-*.jar file in parallelcomputing\lemoine_conjecture\

How to compile and run Sequential:

1. Change Directory to: parallelcomputing\lemoine_conjecture\ 

2. Run: javac src/main/java/edu/rit/cs/Prime.java src/main/java/edu/rit/cs/MyTimer.java src/main/java/edu/rit/cs/LemoineConjecture_Seq.java
       (This will output your class files under the "src/main/java/edu/rit/cs/" directory)
       
3. Run: java src/main/java/edu/rit/cs/LemoineConjecture_Seq.java 1000001 1999999

How to compile and run Parallel:

1. Change Directory to: parallelcomputing\lemoine_conjecture\

2. Run: java -jar omp4j-1.2.jar src/main/java/edu/rit/cs/Prime.java src/main/java/edu/rit/cs/MyTimer.java src/main/java/edu/rit/cs/LemoineConjecture_Par.java
    (This will output your class files under the "lemoine_conjecture" directory)

3. Run: java edu/rit/cs/LemoineConjecture_Par 1000001 1999999

Example Outputs:

Sequential:

    java src\main\java\edu\rit\cs\LemoineConjecture_Seq.java
    1000001 1999999
    1746551 = 1237 + 2*872657
    ElapsedTime (Sequential Lemoine Conjecture): 5.9960627 seconds

Parallel:
    
    java edu/rit/cs/LemoineConjecture_Par
    1000001 1999999
    1746551 = 1237 + 2*872657
    ElapsedTime (Parallel Lemoine Conjecture): 1.6486493 seconds
