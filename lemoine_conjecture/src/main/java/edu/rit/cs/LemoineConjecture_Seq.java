package edu.rit.cs;

/**
 * File: LemoineConjecture_Seq.java
 *
 * @author John Tran - jxt5551@rit.edu
 *
 * This program is a sequential version of the Lemoine Conjecture for CSCI-654 Foundations of Parallel Computing
 * Lemoine Conjecture: Every odd integer greater than 5 is the sum of a prime and twice a prime
 *
 * 7 = 3 + 2*2
 * 9 = 3 + 2*3 = 5 + 2*2
 */
public class LemoineConjecture_Seq {

  /**
   * Iterates over all odd integers n from the lowerBound to the upperBound
   * Finds the smallest prime p such that n = p + 2q, where q is prime
   * If more than one integer yields the same maximum p value, output the one with the largest n value
   *
   * @param lowerBound odd integer greater than 5
   * @param upperBound odd integer greater than or equal to lowerBound
   */
  private static void lemoineConjecture(int lowerBound, int upperBound) {
    int maxN = 0;
    int maxP = 0;
    int maxQ = 0;

    // Use the MyTimer class to track program execution time
    MyTimer sequentialTimer = new MyTimer("Sequential Lemoine Conjecture");
    sequentialTimer.start_timer();

    // Use the Prime class to generate the next prime p to be tested in the Lemoine Conjecture
    Prime.Iterator primes = new Prime.Iterator();

    // Iterate over all odd integers n from the lowerBound to the upperBound
    for (int n = lowerBound; n <= upperBound; n += 2) {
      int p = 0, q = 0, currentP = 0, currentQ;
      primes.restart();

      while (currentP < n) {
        currentP = primes.next(); // Generate next prime to be use
        currentQ = (n - currentP) / 2;
        // If the resulting q value is a prime, we have found the smallest p for this n value
        if (Prime.isPrime(currentQ)) {
          p = currentP;
          q = currentQ;
          currentP = n; // Break the loop without using return (unsupported in OMP4J)
        }
      }

      if (p >= maxP) { // Find p such that p is the largest among n examined
        maxP = p;
        maxQ = q;
        maxN = n;
      }
    }

    System.out.printf("%d = %d + 2*%d\n", maxN, maxP, maxQ);
    sequentialTimer.stop_timer();
    sequentialTimer.print_elapsed_time();
  }


  /**
   * Main program
   *
   * @param args command line arguments from user
   */
  public static void main(String[] args) {
    // Ensure proper number of arguments
    if (args.length < 2) {
      System.out.println("Usage: $ java LemoineConjecture_Seq.java -lowerBound -upperBound");
      return;
    }

    int lowerBound, upperBound;

    // Ensure arguments are integers
    try {
      lowerBound = Integer.parseInt(args[0]);
      upperBound = Integer.parseInt(args[1]);
    } catch (Exception e) {
      System.err.println("Invalid Input! Please make sure you are using integers.");
      return;
    }

    // Ensure boundaries are odd numbers
    if (lowerBound % 2 == 0 || upperBound % 2 == 0) {
      System.err.println("Please make sure you are entering odd numbers!");
      return;
    }

    // Ensure lower bound is greater than 5
    if (lowerBound < 5) {
      System.err.println("Please make sure your lower bound is greater than 5.");
      return;
    }

    // Ensure upper bound is greater than or equal to lower bound
    if (lowerBound > upperBound) {
      System.err
          .println("Please make sure the upper bound is greater than or equal to the lower bound.");
      return;
    }

    lemoineConjecture(lowerBound, upperBound);
  }
}
