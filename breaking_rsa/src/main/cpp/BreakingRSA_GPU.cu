/**
    CSCI-654-01 Foundations of Parallel Computing
    BreakingRSA_GPU.cpp
    Purpose: This program looks for cube roots of ciphertext c with modulus n using a GPU

    @author John Tran - jxt5551@rit.edu
    @version 1.0 11/10/2019
*/
#include <iostream>
#include <algorithm>
using namespace std;

// Accessible by ALL CPU and GPU functions !!!
__managed__ int solIdx = 0; // How many solutions we find
__managed__ unsigned long long int solArray[3]; // Max three solutions
// Kernel function
__global__
/*
 * Compute the modular cube to decrypt the ciphertext if a modular cube exists
 * @param c ciphertext
 * @param n modulus
 */
void computeModularCube(unsigned long long int c, unsigned long long int n) {
    // index = block index * number of threads per block + thread index
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    // stride  = number threads per block * number of block per grid
    int stride = blockDim.x * gridDim.x;

    for (unsigned long long int i = index; i < n; i += stride) {
        unsigned long long int m = ((((i * i) % n) * i) % n);
        if (c == m) {
            atomicAdd(&solArray[solIdx], i);
            solIdx++;
        }
    }
}

// host code
/*
 * Main function
 */
int main(int argc, char* argv[]) {
    unsigned long long int c, n;
    // Check the number of parameters
    if (argc == 3) {
        c = strtoull(argv[1], NULL, 10);
        n = strtoull(argv[2], NULL, 10);

        // Run kernel on some number of points on the GPU
        int blockSize = 256;
        int numBlocks = (c + blockSize - 1) / blockSize;
        computeModularCube<<<numBlocks, blockSize>>>(c, n);

        // Wait for GPU to finish before accessing on host
        cudaDeviceSynchronize();

        // Print results of program
        if (solIdx == 0) {
            cout << "No cube roots of " << c << " (mod " << n << ")" << endl;
        } else {
            sort(solArray, solArray + solIdx);
            for (int i = 0; i < solIdx; i++) {
                cout << solArray[i] << "^3 = " << c << " (mod " << n << ")" << endl;
            }
        }
    } else {
        // Tell the user how to run the program
        cerr << "Usage: " << argv[0] << " -c -n" << endl;
        cerr << "-c: ciphertext" << endl;
        cerr << "-n: modulus" << endl;
        return 1;
    }

    return 0;
}
