/**
    CSCI-654-01 Foundations of Parallel Computing
    BreakingRSA_CPU.cpp
    Purpose: This program looks for cube roots of ciphertext c with modulus n using a CPU

    @author John Tran - jxt5551@rit.edu
    @version 1.0 11/10/2019
*/
#include<iostream>
using namespace std;

/*
 * Compute the modular cube to decrypt the ciphertext if a modular cube exists
 * @param c ciphertext
 * @param n modulus
 */
void computeModularCube(unsigned long long int c, unsigned long long int n) {
    bool solutionFound = false;
    for (unsigned long long int i = 0; i < n - 1; i++) {
        unsigned long long int m = ((((i * i) % n) * i) % n);
        if (c == m) {
            cout << i << "^3 = " << c << " (mod " << n << ")" << endl;
            solutionFound = true;
        }
    }
    if (!solutionFound) {
        cout << "No cube roots of " << c << " (mod " << n << ")" << endl;
    }
}

/*
 * Main function
 */
int main(int argc, char *argv[]) {
    // Using 64-bit integer to prevent overflow causing incorrect results
    unsigned long long int c, n;

    if (argc == 3) {
        c = strtoull(argv[1], NULL, 10);
        n = strtoull(argv[2], NULL, 10);
        computeModularCube(c, n);
    } else {
        cerr << "Usage: " << argv[0] << " -c -n" << endl;
        cerr << "-c: ciphertext" << endl;
        cerr << "-n: modulus" << endl;
        return 1;
    }

    return 0;
}