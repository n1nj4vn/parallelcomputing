package edu.rit.cs;

import mpi.MPI;
import mpi.MPIException;

/**
 * File: LargeParallelTriangle.java
 *
 * @author John Tran - jxt5551@rit.edu
 *
 * This program is a parallel version of the Largest Triangle for CSCI-654 Foundations of Parallel
 * Computing
 *
 * Given a group of two-dimensional points, we want to find the largest triangle, namely three
 * distinct points that are the vertices of a triangle with the largest area. The area of a triangle
 * is (s(s − a)(s − b)(s − c))1/2, where a, b, and c are the lengths of the triangle's sides and s =
 * (a + b + c)/2. The length of a triangle's side is the Euclidean distance between the side's two
 * endpoints.
 */
public class LargeTriangleParallel {
  /**
   * Finds the largest triangle within a set of randomly generated points
   *
   * @param pointArray array of random points generated from number of points with a set seed
   * @param start the start of a set of points to iterate through for working in a cluster
   * @param end the end of a set of points to iterate through for working in a cluster
   */
  private static double[] findLargestTriangle(Point[] pointArray, int start, int end) {
    double area = 0;
    int pointOne = 0, pointTwo = 0, pointThree = 0;

    for (int i = end - 1; i > start; i--) {
      for (int j = i - 1; j > 0; j--) {
        for (int k = j - 1; k > 0; k--) {
          if (areaOfTriangle(pointArray[i], pointArray[j], pointArray[k]) > area) {
            area = areaOfTriangle(pointArray[i], pointArray[j], pointArray[k]);
            pointOne = k;
            pointTwo = j;
            pointThree = i;
          }
        }
      }
    }

    double[] result = new double[4];
    result[0] = area;
    result[1] = pointOne;
    result[2] = pointTwo;
    result[3] = pointThree;

    return result;
  }

  /**
   * Use OpenMPI to find the largest triangle within a set of randomly generated points
   * in parallel in a cluster environment
   *
   * @param numPoints Number of points (Greater than 0)
   * @param side Side of the square region (Greater than 0.0)
   * @param seed Pseudorandom number generator seed
   */
  private void findLargestTriangleParallel(int numPoints, double side, long seed) throws MPIException {
    // Use the MyTimer class to track program execution time
    MyTimer parallelTimer = new MyTimer("Parallel Largest Triangle");
    parallelTimer.start_timer();

    RandomPoints rndPoints = new RandomPoints(numPoints, side, seed);

    Point p;
    Point[] pointArray = new Point[numPoints];

    // Generate random points and store them in the point array pointArray
    int idx = 0;
    while (rndPoints.hasNext()) {
      p = rndPoints.next();
      pointArray[idx] = p;
      idx++;
    }

    int rank = MPI.COMM_WORLD.getRank(), size = MPI.COMM_WORLD.getSize();
//      String hostname = MPI.getProcessorName();
//      System.out.println(
//          "From Java Program: Number of tasks= " + size + ", My rank=" + rank + ", Running on "
//              + hostname);

    int workSize = numPoints / size;
    int[] sendS = new int[size], sendE = new int[size], recvS = new int[1], recvE = new int[1];

    int temp = 0;
    // Chunk up the work based on the number of workers available
    for (int i = 0; i < size - 1; i++) {
      sendS[i] = temp;
      sendE[i] = temp + workSize;
      temp = temp + workSize;
    }
    sendS[size - 1] = temp;
    sendE[size - 1] = pointArray.length;

    // MPI sends chunks of the work array to workers
    // We need to send the start and end values for each worker to findLargestTriangle
    MPI.COMM_WORLD.scatter(sendS, 1, MPI.INT, recvS, 1, MPI.INT, 0);
    MPI.COMM_WORLD.scatter(sendE, 1, MPI.INT, recvE, 1, MPI.INT, 0);

    double[] result = findLargestTriangle(pointArray, recvS[0], recvE[0]);

    // We take the result of this worker and reduce it with MPI to find the max area
    double[] buffOne = {result[0]}, buffTwo = new double[1];
    MPI.COMM_WORLD.allReduce(buffOne, buffTwo, 1, MPI.DOUBLE, MPI.MAX);

    if (result[0] == buffTwo[0]) {
      parallelTimer.stop_timer();
      int pointOne = (int) result[1];
      int pointTwo = (int) result[2];
      int pointThree = (int) result[3];
      System.out.printf("%d %.5g %.5g%n", pointOne + 1, pointArray[pointOne].getX(),
          pointArray[pointOne].getY());
      System.out.printf("%d %.5g %.5g%n", pointTwo + 1, pointArray[pointTwo].getX(),
          pointArray[pointTwo].getY());
      System.out.printf("%d %.5g %.5g%n", pointThree + 1, pointArray[pointThree].getX(),
          pointArray[pointThree].getY());
      System.out.printf("%.5g%n", result[0]);
      parallelTimer.print_elapsed_time();
    }
  }

  /**
   * Calculate the area of a triangle
   *
   * @param pointOne point one
   * @param pointTwo point two
   * @param pointThree point three
   */
  private static double areaOfTriangle(Point pointOne, Point pointTwo, Point pointThree) {
    double a = euclideanDistance(pointTwo, pointOne); // point one to point two
    double b = euclideanDistance(pointThree, pointTwo); // point two to point three
    double c = euclideanDistance(pointOne, pointThree); // point three to point one
    double s = (a + b + c) / 2;

    return Math.sqrt(s * (s - a) * (s - b) * (s - c));
  }

  /**
   * Calculate the euclidean distance between two points
   *
   * @param pointOne first point
   * @param pointTwo second point
   * @return euclidean distance
   */
  private static double euclideanDistance(Point pointOne, Point pointTwo) {
    return Math.sqrt(Math.pow(pointTwo.getY() - pointOne.getY(), 2.0) + Math
        .pow(pointTwo.getX() - pointOne.getX(), 2.0));
  }

  /**
   * Main program
   *
   * @param args command line arguments from user
   */
  public static void main(String[] args) {
    try {
      LargeTriangleParallel res = new LargeTriangleParallel();
      if (args.length != 3) {
        System.out.println("Usage: $ java LargeTriangle.java -N -side -seed");
        System.out.println("\tN: Number of points (Greater than 0)");
        System.out.println("\tside: Side of the square region (Greater than 0.0)");
        System.out.println("\tseed: Pseudorandom number generator seed");
        return;
      }
      MPI.Init(args);

      // Extract arguments from command line arguments
      int numPoints = Integer.parseInt(args[0]);
      double side = Double.parseDouble(args[1]);
      long seed = Long.parseLong(args[2]);

      res.findLargestTriangleParallel(numPoints, side, seed);

      MPI.Finalize();
    } catch (MPIException e) {
      System.err.println(e);
    }
  }
}
