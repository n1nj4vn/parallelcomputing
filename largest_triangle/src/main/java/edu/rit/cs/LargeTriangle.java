package edu.rit.cs;

/**
 * File: LargeTriangle.java
 *
 * @author John Tran - jxt5551@rit.edu
 *
 * This program is a parallel version of the Largest Triangle for CSCI-654 Foundations of Parallel
 * Computing
 *
 * Given a group of two-dimensional points, we want to find the largest triangle, namely three
 * distinct points that are the vertices of a triangle with the largest area. The area of a triangle
 * is (s(s − a)(s − b)(s − c))1/2, where a, b, and c are the lengths of the triangle's sides and s =
 * (a + b + c)/2. The length of a triangle's side is the Euclidean distance between the side's two
 * endpoints.
 */
public class LargeTriangle {

  /**
   * Finds the largest triangle within a set of randomly generated points
   *
   * @param numPoints Number of points (Greater than 0)
   * @param side Side of the square region (Greater than 0.0)
   * @param seed Pseudorandom number generator seed
   */
  private static void findLargestTriangle(int numPoints, double side, long seed) {
    RandomPoints rndPoints = new RandomPoints(numPoints, side, seed);

    Point p;
    Point[] pointArray = new Point[numPoints];

    // Generate random points and store them in the point array pointArray
    int idx = 0;
    while (rndPoints.hasNext()) {
      p = rndPoints.next();
      pointArray[idx] = p;
      // System.out.println(idx + ": x=" + p.getX() + " y=" + p.getY());
      idx++;
    }

    double area = 0;
    int pointOne = 0, pointTwo = 0, pointThree = 0;

    for (int i = numPoints - 1; i > 0; i--) {
      for (int j = i - 1; j > 0; j--) {
        for (int k = j - 1; k > 0; k--) {
          if (areaOfTriangle(pointArray[i], pointArray[j], pointArray[k]) > area) {
            area = areaOfTriangle(pointArray[i], pointArray[j], pointArray[k]);
            pointOne = k;
            pointTwo = j;
            pointThree = i;
          }
        }
      }
    }

    System.out.printf("%d %.5g %.5g%n", pointOne + 1, pointArray[pointOne].getX(),
        pointArray[pointOne].getY());
    System.out.printf("%d %.5g %.5g%n", pointTwo + 1, pointArray[pointTwo].getX(),
        pointArray[pointTwo].getY());
    System.out.printf("%d %.5g %.5g%n", pointThree + 1, pointArray[pointThree].getX(),
        pointArray[pointThree].getY());
    System.out.printf("%.5g%n", area);
  }

  /**
   * Calculate the area of a triangle
   *
   * @param pointOne point one
   * @param pointTwo point two
   * @param pointThree point three
   */
  private static double areaOfTriangle(Point pointOne, Point pointTwo, Point pointThree) {
    double a = euclideanDistance(pointTwo, pointOne); // point one to point two
    double b = euclideanDistance(pointThree, pointTwo); // point two to point three
    double c = euclideanDistance(pointOne, pointThree); // point three to point one
    double s = (a + b + c) / 2;

    return Math.sqrt(s * (s - a) * (s - b) * (s - c));
  }

  /**
   * Calculate the euclidean distance between two points
   *
   * @param pointOne first point
   * @param pointTwo second point
   * @return euclidean distance
   */
  private static double euclideanDistance(Point pointOne, Point pointTwo) {
    return Math.sqrt(Math.abs(Math.pow(pointOne.getY() - pointTwo.getY(), 2.0) + Math
        .pow(pointOne.getX() - pointTwo.getX(), 2.0)));
  }

  /**
   * Main program
   *
   * @param args command line arguments from user
   */
  public static void main(String[] args) {
    // Use the MyTimer class to track program execution time
    MyTimer sequentialTimer = new MyTimer("Sequential Largest Triangle");
    sequentialTimer.start_timer();

    if (args.length != 3) {
      System.out.println("Usage: $ java LargeTriangle.java -N -side -seed");
      System.out.println("\tN: Number of points (Greater than 0)");
      System.out.println("\tside: Side of the square region (Greater than 0.0)");
      System.out.println("\tseed: Pseudorandom number generator seed");
      return;
    }

    // Extract arguments from command line arguments
    int numPoints = Integer.parseInt(args[0]);
    double side = Double.parseDouble(args[1]);
    long seed = Long.parseLong(args[2]);

    findLargestTriangle(numPoints, side, seed);

    sequentialTimer.stop_timer();
    sequentialTimer.print_elapsed_time();
  }
}
