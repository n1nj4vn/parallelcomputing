<h2>Largest Triangle</h2>
<p><a name="system"></a></p>
<p>Given a group of two-dimensional points, we want to find the&nbsp;<strong>largest triangle,</strong>&nbsp;namely three distinct points that are the vertices of a triangle with the largest area. The area of a triangle is (<em>s</em>(<em>s</em>&nbsp;&minus;&nbsp;<em>a</em>)(<em>s</em>&nbsp;&minus;&nbsp;<em>b</em>)(<em>s</em>&nbsp;&minus;&nbsp;<em>c</em>))<sup>1/2</sup>, where&nbsp;<em>a</em>,&nbsp;<em>b</em>, and&nbsp;<em>c</em>&nbsp;are the lengths of the triangle's sides and&nbsp;<em>s</em>&nbsp;=&nbsp;(<em>a</em>&nbsp;+&nbsp;<em>b</em>&nbsp;+&nbsp;<em>c</em>)/2. The length of a triangle's side is the Euclidean distance between the side's two endpoints.</p>
<p>You will write sequential and cluster parallel versions of a program that finds the largest triangle in a group of points. The program's command line argument is a&nbsp;<em>constructor expression</em>&nbsp;for a PointSpec object from which the program obtains the points' (<em>x,y</em>) coordinates. </p>
<p>John Tran - Results:</p>

<blockquote>
jxt5551@tardis:~/Courses/CSCI654/parallelcomputing/largest_triangle/src/main/java$ java edu.rit.cs.LargeTriangle 3000 100 142857<br />
<br />
1577 0.0038473 79.791<br />
2489 99.531 99.740<br />
2924 96.984 0.14588<br />
4930.8<br />
ElapsedTime (Sequential Largest Triangle): 39.940381966 seconds
</blockquote>

<blockquote>
jxt5551@tardis:~/Courses/CSCI654/parallelcomputing/largest_triangle/src/main/java$ mpirun --hostfile /usr/local/pub/ph/TardisCluster3Nodes.txt --prefix /usr/local java -cp /usr/local/pub/ph/mpi.jar edu.rit.cs.LargeTriangleParallel 3000 100 142857<br />
<br />
1577 0.0038473 79.791<br />
2489 99.531 99.740<br />
2924 96.984 0.14588<br />
4930.8<br />
ElapsedTime (Parallel Largest Triangle): 8.570338462 seconds
</blockquote>